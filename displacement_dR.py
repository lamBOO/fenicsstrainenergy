import fenics as fe
import matplotlib.pyplot as plt
import numpy as np
from math import pi, sqrt
from numpy.lib.arraysetops import _setdiff1d_dispatcher
import ufl

# --------------------
# Functions and classes
# --------------------
def left(x, on_boundary):
    return (x[0] < -1.98)

# Strain function
def epsilon(u):
    return fe.sym(fe.grad(u))

# Stress function
def sigma(u):
    i, j, k, l = ufl.indices(4)
    delta = fe.Identity(2)
    delta4 = ufl.as_tensor(1.0/2.0*(delta[i,k] * delta[j,l] + delta[i,l] * delta[j,k]),(i,j,k,l))

    Cijkl = (
          lambda_ * delta[i,j] * delta[k,l]
        + 2.0 * mu * delta4[i,j,k,l]
        + alpha_R * (ufl.outer(M_R, delta) + ufl.outer(delta, M_R))[i,j,k,l]
        + 2.0 * mu * 1.0/2.0 *
                   (ufl.outer(d_R, d_R)[i,k]*delta[j,l]
                  + ufl.outer(d_R, d_R)[i,l]*delta[j,k]
                  + ufl.outer(d_R, d_R)[l,j]*delta[i,k]
                  + ufl.outer(d_R, d_R)[k,j]*delta[i,l]
                   )
        + beta_R * (ufl.outer(M_R, M_R))[i,j,k,l]
    )
    C = ufl.as_tensor(Cijkl,(i,j,k,l))

    sigma_ij = C[i,j,k,l] * epsilon(u)[k,l]
    sigma = ufl.as_tensor(sigma_ij, (i, j))
    return sigma

    # working version, initial w.o. Einstein
    return lambda_*fe.div(u)*fe.Identity(2) + 2*mu*epsilon(u)

# --------------------
# Parameters
# --------------------

#Angle of the direction
angle_R = pi/2.0
d_R = ufl.as_vector([ufl.cos(angle_R),ufl.sin(angle_R)])
M_R = ufl.outer(d_R, d_R)

alpha_R = 1e7 #increase alpha_R will increase total strain energy
beta_R  = 1e6

# Density
rho = fe.Constant(3.85e6)

# Young's modulus and Poisson's ratio
E = 2e11
nu = 0.3

# Lame's constants
lambda_ = (E*nu)/(1.0+nu)/(1.0-2.0*nu)
mu = E/2.0/(1.0+nu)

# Load
g_val = 5e4
b_val = 12
g = fe.Constant((0.0, -g_val))
b = fe.Constant((-b_val, 0.0))

# Model type
model = "plane_strain"
if model == "plane_stress":
    lambda_ = 2*mu*lambda_/(lambda_+2*mu)

# --------------------
# Geometry
# --------------------
# mesh = fe.Mesh("external_mesh.xml")  ######
mesh = fe.Mesh("disk.xml")

# Definition of Neumann condition domain
boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
boundaries.set_all(0)

top = fe.AutoSubDomain(lambda x: x[1] > +1)
bot = fe.AutoSubDomain(lambda x: x[1] < -1)

top.mark(boundaries, 1)
bot.mark(boundaries, 2)
ds = fe.ds(subdomain_data=boundaries)

# --------------------
# Function spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 1)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)

# --------------------
# Boundary conditions
# --------------------
bc = fe.DirichletBC(V, fe.Constant((0.0, 0.0)), left, method='pointwise')
# bc = fe.DirichletBC(V.sub(0),fe.Constant(0),left,method='pointwise')

# --------------------
# Weak form
# --------------------
a = fe.inner(sigma(u_tr), epsilon(u_test))*fe.dx
l = (
  rho*fe.dot(b, u_test)*fe.dx
  + fe.inner(+g, u_test)*ds(1)
  + fe.inner(-g, u_test)*ds(2)
)

# --------------------
# Solver
# --------------------
u = fe.Function(V)
A_ass, L_ass = fe.assemble_system(a, l, bc)

print("Start solve system:")
fe.solve(A_ass, u.vector(), L_ass)
print("Finish solve system:")

print("Maximum displacement:")
print(np.amax(u.vector()[:]))

# --------------------
# Post-process
# --------------------
fe.plot(u, mode ="displacement")
plt.show()

print("Write results to results.pvd")
u.rename("u", "u")
fe.File("u.pvd").write(u, 0)

#Strain epsilon
strain_el = fe.TensorElement("Lagrange", mesh.ufl_cell(), 1)
strain_space = fe.FunctionSpace(mesh, strain_el)
strain_proj = fe.project(epsilon(u), strain_space)
strain_proj.rename("epsilon", "epsilon")  # make PVD naming unique
fe.File("epsilon.pvd").write(strain_proj, 0)

#Stress sigma
stress_el = fe.TensorElement("Lagrange", mesh.ufl_cell(), 1)
stress_space = fe.FunctionSpace(mesh, stress_el)
stress_proj = fe.project(sigma(u), stress_space)
stress_proj.rename("sigma", "sigma")  # make PVD naming unique
fe.File("sigma.pvd").write(stress_proj, 0)
