// Command line Parameters
If(!Exists(p))
  p = 1;
EndIf

// Settings
res = 100;
Mesh.CharacteristicLengthMax = 100000;
Mesh.MshFileVersion = 2.0;

// Parameters
R1 = 2.0;

Point(1) = {0, 0, 0, res};

Point(1001) = {-R1, 0, 0, res};
Point(1002) = {-R1*Sin(Pi/4), -R1*Cos(Pi/4), 0, res};
Point(1003) = {0, -R1, 0, res};
Point(1004) = {R1*Sin(Pi/4), -R1*Cos(Pi/4), 0, res};
Point(1005) = {R1, 0, 0, res};
Point(1006) = {R1*Sin(Pi/4), R1*Cos(Pi/4), 0, res};
Point(1007) = {0, R1, 0, res};
Point(1008) = {-R1*Sin(Pi/4), R1*Cos(Pi/4), 0, res};

Circle(2001) = {1001,1,1002}; Transfinite Curve {2001} = 3;
Circle(2002) = {1002,1,1003}; Transfinite Curve {2002} = 3;
Circle(2003) = {1003,1,1004}; Transfinite Curve {2003} = 3;
Circle(2004) = {1004,1,1005}; Transfinite Curve {2004} = 3;
Circle(2005) = {1005,1,1006}; Transfinite Curve {2005} = 3;
Circle(2006) = {1006,1,1007}; Transfinite Curve {2006} = 3;
Circle(2007) = {1007,1,1008}; Transfinite Curve {2007} = 3;
Circle(2008) = {1008,1,1001}; Transfinite Curve {2008} = 3;

Line(2101) = {1001,1}; Transfinite Curve {2101} = 3;
Line(2102) = {1003,1}; Transfinite Curve {2102} = 3;
Line(2103) = {1005,1}; Transfinite Curve {2103} = 3;
Line(2104) = {1007,1}; Transfinite Curve {2104} = 3;

Line Loop(3001) = {2001,2002,2102,-2101};
Plane Surface(4001) = {3001};
Transfinite Surface {4001} = {1001,1002,1003,1};
Line Loop(3002) = {2003,2004,2103,-2102};
Plane Surface(4002) = {3002};
Transfinite Surface {4002} = {1003,1004,1005,1};
Line Loop(3003) = {2005,2006,2104,-2103};
Plane Surface(4003) = {3003};
Transfinite Surface {4003} = {1005,1006,1007,1};
Line Loop(3004) = {2007,2008,2101,-2104};
Plane Surface(4004) = {3004};
Transfinite Surface {4004} = {1007,1008,1001,1};

Mesh 2; // Mesh 2D
For i In {1:p}
  RefineMesh;
EndFor
