// Command line Parameters
If(!Exists(p))
  p = 2;
EndIf

// Settings
res = 100;
Mesh.CharacteristicLengthMax = 1.0 * 2^(-p);
Mesh.MshFileVersion = 2.0;

// Parameters
R1 = 2.0;

Point(1) = {0, 0, 0, res};

Point(1000) = { R1, 0, 0, res};
Point(1001) = {-R1, 0, 0, res};
Physical Point("left",1000) = {1001};

Circle(2000) = {1000,1,1001};
Circle(2001) = {1001,1,1000};

Line Loop(3000) = {2000}; Physical Curve("top",3000) = {2000};
Line Loop(3001) = {2001}; Physical Curve("bot",3001) = {2001};

Plane Surface(4000) = {3000,3001}; Physical Surface("mesh",4000) = {4000};
