# Strain Energy Test with FEniCS

## Usage

- Install and start Docker

```bash
docker-compose run --rm fenicsr13_release
python3 displacement_test.py
python3 displacement_dRdE.py
# Start solve system:
# Finish solve system:
# Maximum displacement:
# 0.00239569266131
# Write results to results.pvd
```

## Meshing

```bash
# Create disk.geo
# geoToH5 disk.geo disk.h5  # creates ONLY disk.h5 (both needef for FEniCS)
gmsh -2 disk.geo -o disk.msh  # meshing
gmsh -2 disk_structured.geo -o disk_structured.msh  # meshing
dolfin-convert disk.msh disk.xml  # Creates ALSO the xml file (better for this project)
dolfin-convert disk_structured.msh disk_structured.xml  # Creates ALSO the xml file (better for this project)
```

## Summary:

- Dirichlet BC: Left side is fixed
- Neumann BC:
  - Push from top down
  - Push from bottom up
- Gravitational force from right to left (like a bodyforce)
- Strong form:
```math
\begin{aligned}
    \nabla \cdot \mathbb{C}^{(\star)} \nabla_{s}\textbf{u} + \rho \textbf{b} = \textbf{0}
\end{aligned}
```

where $`\mathbb{C}^{(\star)}`$ has the following form

```math
\begin{aligned}
\mathbb{C}_{i j k l}^{d^{R}} \triangleq \lambda & 1 \otimes 1+2 \mu \mathbb{I} \\
&+\alpha^{R}\left(M^{R} \otimes \mathbf{1}+\mathbf{1} \otimes \boldsymbol{M}^{R}\right)
 + \delta^{R} \mathbb{I}_{\boldsymbol{d}^{R}}
 + \beta^{R} \left( \boldsymbol{M}^{R} \otimes \boldsymbol{M}^{R} \right)
\end{aligned}
```
## Visualization of vertorial unknown displacement solution
![alt text](./example.png)

#### Directional effect $`\textbf{d}^{R}`$ off-set with angle $`30^{o}`$
![alt text](./Gallery/Angle_30_degree.png)

#### Directional effect $`\textbf{d}^{R}`$ off-set with angle $`60^{o}`$
![alt text](./Gallery/Angle_60_degree.png)

#### Directional effect $`\textbf{d}^{R}`$ off-set with angle $`90^{o}`$
![alt text](./Gallery/Angle_90_degree.png)
